import { Button, Col, Form, Input, Row, Typography, message } from "antd";
import React, { useState } from "react";
import axios from "axios";
import { PATTERN, TOKEN_CYBERSOFT } from "../../constants";
import { setLocalStorage } from "../../utils/localStorage";
import { useNavigate } from "react-router-dom";

const LoginPage = () => {
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const handleSubmit = async (values) => {
    try {
      setIsLoading(true);
      const { taiKhoan, matKhau } = values;
      const formData = { taiKhoan, matKhau };
      const { data } = await axios.post(
        "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
        formData,
        {
          headers: {
            TokenCybersoft: TOKEN_CYBERSOFT,
          },
        }
      );

      console.log("values: ", data);
      if (data.statusCode == 200 && data.content.accessToken) {
        const currentUser = {
          hoTen: data.content.hoTen,
          email: data.content.email,
          maLoaiNguoiDung: data.content.maLoaiNguoiDung,
          taiKhoan: data.content.taiKhoan,
          soDT: data.content.soDT,
        };
        setLocalStorage("accessToken", data.content.accessToken);
        setLocalStorage("currentUser", currentUser);
        navigate("/list-movie");
      }
    } catch (error) {
      console.log('error: ', error);
      message.error("Đã có lỗi xảy ra ");
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <div className="container">
      <Typography
        style={{
          textAlign: "center",
          fontWeight: "700",
          fontSize: "32",
          margin: "30px 0",
        }}
      >
        LoginPage
      </Typography>
      <Row justify={"center"}>
        <Col span={16}>
          <Form
            onFinish={handleSubmit}
            onFinishFailed={() => {
              message.error("Lỗi Rồi Má Ơi");
            }}
          >
            <Form.Item
              name="taikhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input
                name="taikhoan"
                placeholder="Tài Khoản"
                size="large"
              ></Input>
            </Form.Item>
            <Form.Item
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your Password!",
                },
                // {
                //   pattern: PATTERN.PASSWORD,
                //   message:
                //     "Mật khẩu ít nhất 8 ký tự và bao gồm 1 ký tự viết hoa",
                // },
              ]}
            >
              <Input.Password
                name="matKhau"
                placeholder="Mật Khẩu"
                size="large"
              ></Input.Password>
            </Form.Item>
            <div style={{ textAlign: "end" }}>
              <Button
                loading={isLoading}
                htmlType="submit"
                type="primary"
                size="large"
              >
                Đăng Nhập
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </div>
  );
};

export default LoginPage;
